<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/images', 'ImageController@getImages')->name('images');
Route::post('/upload', 'ImageController@postUpload')->name('uploadfile');

Route::get('/users', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::delete('/users/{id}', 'UserController@destroy');
