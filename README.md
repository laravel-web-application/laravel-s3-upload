![Laravel Logo](img/laravel.png "Laravel Logo")

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel S# Upload
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-s3-upload.git`
2. Go inside the folder: `cd laravel-s3-upload`
3. Run `cp .env.example .env` then put your database name, credentials & AWS Key.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

### Image Screen shot

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Upload Page

![Upload Page](img/upload1.png "Upload Page")

![Upload Page](img/upload2.png "Upload Page")

![S3 Upload Page](img/s3.png "S3 Upload Page")

User Avatar

![User Avatar](img/add.png)

![Delete User Avatar](img/delete.png)

Link Article --> https://daengweb.id/cara-upload-file-ke-amazon-s3-menggunakan-laravel-7
